WeFix are highly qualified Boiler, Bathroom, Kitchen and general refurbishment specialists based in Richmond upon Thames.  With over 30 years� experience in the industry we pride ourselves on delivering high quality customer service combined with rapid response times.

Address: Causeway House, 13 The Causeway, Teddington, TW11 0JR, UK

Phone: +44 20 8948 1688